var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};

var ValidatorExtended = (function (_super) {
    __extends(ValidatorExtended, _super);
    function ValidatorExtended() {
        _super.apply(this, arguments);
    };

    ValidatorExtended.notNull = function (str) {
        return !ValidatorExtended.isNull(str);
    };

    ValidatorExtended.notEmpty = function (str) {
        return ValidatorExtended.isLength(str, 1);
    };

    ValidatorExtended.isEmpty = function (str) {
        return ValidatorExtended.isLength(str, 0, 0);
    };

    return ValidatorExtended;
})(require('validator'));
exports.ValidatorExtended = ValidatorExtended;
