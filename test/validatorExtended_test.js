var assert = require("assert");
var validator = require("./../ValidatorExtended").ValidatorExtended;

/**
 * Server side tests.
 */
describe('ValidatorExtended', function(){
    describe('#notNull()', function(){
        it('should return true when a value is present', function(){
            assert.equal(true, validator.notNull('string'));
            assert.equal(true, validator.notNull('0'));
            assert.equal(true, validator.notNull(' '));
            assert.equal(true, validator.notNull(0));
        });

        it('should return false when there is no value', function(){
            assert.equal(false, validator.notNull(''));
            assert.equal(false, validator.notNull(null));
        });
    });

    describe('#notEmpty()', function(){
        it('should return true when a value is present', function(){
            assert.equal(true, validator.notEmpty('string'));
            assert.equal(true, validator.notEmpty('0'));
            assert.equal(true, validator.notEmpty(0));
            assert.equal(true, validator.notEmpty(' '));
        });

        it('should return false when there is no value', function(){
            assert.equal(false, validator.notEmpty(''));
            assert.equal(false, validator.notEmpty(null));
        });
    });

    describe('#isEmpty()', function(){
        it('should return true when a value is present', function(){
            assert.equal(true, validator.isEmpty(''));
            assert.equal(true, validator.isEmpty(null));
        });

        it('should return false when there is no value', function(){
            assert.equal(false, validator.isEmpty('0'));
            assert.equal(false, validator.isEmpty(0));
            assert.equal(false, validator.isEmpty(' '));
        });
    });
});