Validator-Extended
=======================

###validator.js###
* **Based on** [validator.js](https://github.com/chriso/validator.js)
* _Current version based on:_ _**4.2.1**_

```javascript
var validator = require('validatorExtended');
```

###node-validator-extended###
The main purpose of this repo is to add redundancy functions that won't be added in the validator.js repository.
These functions could be useful for some people.

####Validators####
The following functions have been added to the original Validator:

* **notNull(str): boolean** - Check if string is not null.
* **notEmpty(str): boolean** - Check if string is not empty.
* **isEmpty(str): boolean** - Check if string is empty.

#### Tests
We now use mocha to perform tests.

`npm install mocha -g`

To run the test, just use **mocha**, *assuming you're on the directory*:
`mocha`

```shell
C:\wamp\www\Ayolan\Validator-extended>mocha
    ValidatorExtended
        #notNull()
          V should return true when a value is present
          V should return false when there is no value
        #notEmpty()
          V should return true when a value is present
          V should return false when there is no value
        #isEmpty()
          V should return true when a value is present
          V should return false when there is no value

```

#### The library isn't up-to-date?
The official `validator` library updates itself quite often, so I am most of the time *out-of-date*.
In order to provide the most recent version I have decided to write in the `package.json` the following dependency: `"validator": "latest"`.
So you can just do `npm update validator-extended` and it will install the latest `validator` version in `node_modules/validator`.

*But it won't work for the browser version, which requires a manual update from me and a new publish on **npm**. In this case just create an issue to ask for the latest version! I'll do it ASAP.*

####Licence####
https://github.com/Ayolan/node-validator-extended/blob/master/LICENSE

*You can obviously fork and add other functions. Please just check if they should not belong to the **[validator.js](https://github.com/chriso/validator.js)** repository before! :)*